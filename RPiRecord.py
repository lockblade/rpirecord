import time
import pyaudio
import RPi.GPIO as GPIO
import wave


#Setup GPIO
#Pull up resistor on pin 11, make sure to connect switch to ground.
#LED on pin 12, with 750ohm resistor
GPIO.setmode(GPIO.BOARD)
GPIO.setup(12, GPIO.OUT)
GPIO.setup(11, GPIO.IN, pull_up_down=GPIO.PUD_UP)

#Set up audio stuff
CHUNK =		1024
WIDTH = 	2
CHANNELS = 	2
RATE =		48000
rec = False
chunkNum = 0
p = pyaudio.PyAudio()
frames = []

#Button detection code lol

def eventCallback(channel, inData, frameNum, timeInfo, status):
	if GPIO.input(channel):
		print("Button pushed!")
#		rec = not rec
		GPIO.remove_event_detect(channel)
		time.sleep(1)
		GPIO.add_event_detect(channel, GPIO.FALLING, callback=eventCallback(channel), bouncetime=300)


def writeFile(data):
	wavFile = wave.open(time.time(), 'wb')
	wavFile.setnchannels(CHANNELS)
	wavFile.setsamplewidth(p.get_sample_size(p.get_format_from_width(WIDTH)))
	wavFile.setsamplerate(RATE)
	wavFile.writeframes(b''.join(data))
	wavFile.close()


#Main Loop
while True:
	if rec:
		frames.append(stream.read(CHUNK))
	else:
		time.sleep(.05)
