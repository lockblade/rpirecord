import time
import pyaudio
import wave

#Set up audio stuff
rec = False
chunkNum = 0
p = pyaudio.PyAudio()
data = []
CHUNK =		1024
WIDTH = 	2
CHANNELS = 	2
RATE =		48000
TIME = 5

rec = False

stream=p.open(format=p.get_format_from_width(WIDTH),
			  channels=CHANNELS, rate = RATE,
			  input=True, frames_per_buffer=CHUNK)
def stoprec():
	stream.stop_stream()
	wavFile = wave.open(time.time(), 'w')
	wavFile.setnchannels(CHANNELS)
	wavFile.setsampwidth(p.get_sample_size(p.get_format_from_width(WIDTH)))
	wavFile.setframerate(RATE)
	wavFile.writeframes(b''.join(data))
	wavFile.close()
	stream.start_stream()
	data = []

data = []

print("Recording 1")
for i in range(0, int(RATE/CHUNK * TIME)):
	data.append(stream.read(CHUNK))
print("Stopping Recording")
stoprec()
print("Recording 2")
for i in range(0,int(RATE/CHUNK*TIME)):
	data.append(stream.read(CHUNK))
print("Stopping Recording")
stoprec()