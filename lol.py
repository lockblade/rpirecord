import RPi.GPIO as GPIO, time

state = 1
prev = 1
toggle = False

GPIO.setmode(GPIO.BOARD)
GPIO.setup(11, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(12, GPIO.OUT)
GPIO.output(12, 0)

while True:
	state = GPIO.input(11)
	if state != prev:
		prev = state
		if toggle:
			GPIO.output(12, 1)
			toggle = False
		else:
			GPIO.output(12, 0)
			toggle = True
	else:
		print("No Change.")
	time.sleep(.1)
